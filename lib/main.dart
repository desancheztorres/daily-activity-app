import 'package:daily_activity_app/daily_activity_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(DailyActivityApp());

class DailyActivityApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DailyActivityPage(),
    );
  }
}
